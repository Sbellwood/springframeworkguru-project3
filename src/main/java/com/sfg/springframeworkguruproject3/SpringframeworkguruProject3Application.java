package com.sfg.springframeworkguruproject3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringframeworkguruProject3Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringframeworkguruProject3Application.class, args);
    }

}
